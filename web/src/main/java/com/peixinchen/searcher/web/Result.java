package com.peixinchen.searcher.web;

import lombok.Data;

@Data
public class Result {
    private final String title;
    private final String url;
    private final String desc;
}
