package com.peixinchen.searcher.web;

import lombok.Data;

@Data
public class Document {
    private int docId;
    private String title;
    private String url;
    private String content;
}
