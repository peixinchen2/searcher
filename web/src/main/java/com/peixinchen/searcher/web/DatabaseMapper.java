package com.peixinchen.searcher.web;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface DatabaseMapper {
    List<Document> query(@Param("query") String query, @Param("offset") int offset, @Param("limit") int limit);
}
