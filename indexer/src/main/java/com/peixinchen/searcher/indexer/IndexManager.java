package com.peixinchen.searcher.indexer;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class IndexManager {
    private final IndexProperties properties;
    private final ExecutorService executorService;
    private final IndexDatabaseMapper mapper;

    @Autowired
    public IndexManager(IndexProperties properties, ExecutorService executorService, IndexDatabaseMapper mapper) {
        this.properties = properties;
        this.executorService = executorService;
        this.mapper = mapper;
    }

    @SneakyThrows
    public void addForwardIndex(List<Document> documentList) {
        int batchSize = properties.getForwardIndexBatchInsertSize();
        int listSize = documentList.size();
        int countDown = (int) Math.ceil(listSize * 1.0 / batchSize);
        log.info("保存正排索引需要提交 {} 批任务。", countDown);
        CountDownLatch latch = new CountDownLatch(countDown);
        AtomicInteger complete = new AtomicInteger(0);
        for (int i = 0; i < listSize; i += batchSize) {
            int from = i;
            int to = from + batchSize;

            Runnable runnable = () -> {
                List<Document> subList = documentList.subList(from, to);
                int count = mapper.batchInsertForwardIndex(subList);
                int c = complete.addAndGet(count);
                // log.info("插入正排索引 {} 个，一共 {} 个。", c, listSize);
                latch.countDown();
            };

            executorService.submit(runnable);
        }

        latch.await();
    }

    @SneakyThrows
    public void addInvertedIndex(List<Document> documentList) {
        int batchSize = properties.getInvertedIndexBatchInsertSize();
        int groupSize = properties.getInvertedIndexBatchInsertGroupSize();
        int listSize = documentList.size();
        int countDown = (int) Math.ceil(listSize * 1.0 / groupSize);
        log.info("保存正排索引需要提交 {} 批任务。", countDown);
        CountDownLatch latch = new CountDownLatch(countDown);

        for (int i = 0; i < listSize; i += groupSize) {
            int from = i;
            int to = from + groupSize;

            Runnable runnable = () -> {
                List<Document> subList = documentList.subList(from, to);
                List<InvertedRecord> list = new ArrayList<>();

                for (Document document : subList) {
                    Map<String, Integer> wordToWeight = document.segWordAndCalcWeight();
                    Set<Map.Entry<String, Integer>> entries = wordToWeight.entrySet();
                    for (Map.Entry<String, Integer> entry : entries) {
                        String word = entry.getKey();
                        int weight = entry.getValue();

                        InvertedRecord record = new InvertedRecord(word, document.getDocId(), weight);
                        list.add(record);
                        if (list.size() == batchSize) {
                            mapper.batchInsertInvertedIndex(list);
                            // log.info("提交一次倒排索引记录，一共 {} 个。", list.size());
                            list.clear();
                        }
                    }
                }

                mapper.batchInsertInvertedIndex(list);
                // log.info("提交一次倒排索引记录，一共 {} 个。", list.size());
                list.clear();
                latch.countDown();
            };

            executorService.submit(runnable);
        }

        latch.await();
    }
}
