package com.peixinchen.searcher.indexer;

import lombok.Data;

@Data
public class InvertedRecord {
    private final String word;
    private final int docId;
    private final int weight;
}
