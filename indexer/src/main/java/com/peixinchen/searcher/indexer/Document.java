package com.peixinchen.searcher.indexer;

import lombok.*;
import org.ansj.domain.Result;
import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.util.*;
import java.util.stream.Collectors;

@ToString
@EqualsAndHashCode
public class Document {
    @Getter @Setter
    private Integer docId;
    @Getter
    private final String title;
    @Getter
    private final String url;
    @Getter
    private final String content;
    private final Map<String, Integer> titleWordCountMap = new HashMap<>();
    private final Map<String, Integer> contentWordCountMap = new HashMap<>();

    public Document(String title, String url, String content) {
        this.title = title;
        this.url = url;
        this.content = content;
    }

    public Map<String, Integer> segWordAndCalcWeight() {
        segTitleWord();
        segContentWord();

        Set<String> wordSet = new HashSet<>(titleWordCountMap.keySet());
        wordSet.addAll(contentWordCountMap.keySet());
        Map<String, Integer> wordToWeight = new HashMap<>();
        for (String word : wordSet) {
            int weight = calcWeight(word);
            wordToWeight.put(word, weight);
        }

        return wordToWeight;
    }

    private int calcWeight(String word) {
        int countInTitle = titleWordCountMap.getOrDefault(word, 0);
        int countInContent = contentWordCountMap.getOrDefault(word, 0);

        return countInTitle * 10 + countInContent;
    }

    private static final Set<String> ignoredNatureStrSet;

    static {
        ignoredNatureStrSet = new HashSet<>();
        ignoredNatureStrSet.add("w");
    }

    private void segContentWord() {
        segAndCount(title, titleWordCountMap);
    }

    private void segTitleWord() {
        segAndCount(content, contentWordCountMap);
    }

    private void segAndCount(String s, Map<String, Integer> map) {
        Result result = ToAnalysis.parse(s);
        List<Term> termList = result.getTerms();
        List<String> wordList = termList.stream()
                .filter(term -> !ignoredNatureStrSet.contains(term.getNatureStr()))
                .map(Term::getName)
                .collect(Collectors.toList());

        for (String word : wordList) {
            int count = map.getOrDefault(word, 0);
            map.put(word, count + 1);
        }
    }
}
