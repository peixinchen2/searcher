package com.peixinchen.searcher.indexer;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties("index")
public class IndexProperties {
    private String docRootPath;
    private String urlPrefix;
    private int forwardIndexBatchInsertSize;
    private int invertedIndexBatchInsertSize;
    private int invertedIndexBatchInsertGroupSize;
}
