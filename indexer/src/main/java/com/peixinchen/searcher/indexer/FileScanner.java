package com.peixinchen.searcher.indexer;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class FileScanner {
    @SneakyThrows
    public List<File> scan(File rootFile, FileFilter filter) {
        List<File> resultList = new ArrayList<>();

        traversal(rootFile, filter, resultList);

        return resultList;
    }

    @SneakyThrows
    private void traversal(File directory, FileFilter filter, List<File> resultList) {
        File[] files = directory.listFiles();
        if (files == null) {
            return;
        }

        for (File file : files) {
            if (filter.accept(file)) {
                resultList.add(file);
            }
        }

        for (File file : files) {
            if (file.isDirectory()) {
                traversal(file, filter, resultList);
            }
        }
    }
}
