package com.peixinchen.searcher.indexer;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class DocumentBuilder {
    private static final String SUFFIX = ".html";

    private final IndexProperties properties;

    @Autowired
    public DocumentBuilder(IndexProperties properties) {
        this.properties = properties;
    }

    public Document build(File rootFile, File docFile) {
        String title = parseTitle(docFile);
        String url = parseUrl(rootFile, docFile);
        String content = parseContent(docFile);

        return new Document(title, url, content);
    }

    @SneakyThrows
    private String parseTitle(File file) {
        String name = file.getName();
        return name.substring(0, name.length() - SUFFIX.length());
    }

    @SneakyThrows
    private String parseUrl(File rootFile, File docFile) {
        String rootPath = rootFile.getCanonicalPath().replace('\\', '/');
        String docPath = docFile.getCanonicalPath().replace('\\', '/');
        String relativePath = docPath.substring(rootPath.length());

        if (properties.getUrlPrefix().endsWith("/")) {
            return properties.getUrlPrefix() + relativePath.substring(1);
        } else {
            return properties.getUrlPrefix() + relativePath;
        }
    }

    @SneakyThrows
    private String parseContent(File file) {
        StringBuilder contentBuilder = new StringBuilder();
        try (InputStream is = new FileInputStream(file)) {
            try (Scanner scanner = new Scanner(is, "ISO-8859-1")) {
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();

                    contentBuilder.append(line).append(" ");
                }
            }
        }

        return contentBuilder.toString()
                .replaceAll("<script.*?>.*?</script>", " ") // 去掉 <script ...>...</script>
                .replaceAll("<[^>]*>", " ")                     // 去掉所有标签 <...>
                .replaceAll("&.*?;", " ")                        // 去掉 HTML 转义符
                .replaceAll("\\s+", " ")                        // 合并空白字符
                .trim();                                                          // 去掉首尾空白字符
    }
}
