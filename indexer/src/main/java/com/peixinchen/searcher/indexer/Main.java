package com.peixinchen.searcher.indexer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

@Slf4j
@Component
public class Main implements CommandLineRunner {
    private final IndexProperties properties;
    private final FileScanner fileScanner;
    private final DocumentBuilder documentBuilder;
    private final IndexManager indexManager;
    private final ExecutorService executorService;

    @Autowired
    public Main(IndexProperties properties, FileScanner fileScanner, DocumentBuilder documentBuilder, IndexManager indexManager, ExecutorService executorService) {
        this.properties = properties;
        this.fileScanner = fileScanner;
        this.documentBuilder = documentBuilder;
        this.indexManager = indexManager;
        this.executorService = executorService;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("配置信息: {}", properties);

        File rootFile = new File(properties.getDocRootPath());

        log.info("开始创建索引，文档目录: {}。", rootFile.getCanonicalPath());
        List<File> resultList = fileScanner.scan(rootFile, file -> file.isFile() && file.getName().endsWith(".html"));
        log.info("扫描出 html 文件共 {} 个。", resultList.size());

        List<Document> documentList = resultList.stream()
                .parallel()
                .map(file -> documentBuilder.build(rootFile, file))
                .collect(Collectors.toList());
        log.info("文档构建完成，共 {} 个。", documentList.size());

        indexManager.addForwardIndex(documentList);
        log.info("正排索引构建完成。");

        indexManager.addInvertedIndex(documentList);
        log.info("倒排索引构建完成。");

        executorService.shutdown();
    }
}
