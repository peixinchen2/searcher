package com.peixinchen.searcher.indexer;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface IndexDatabaseMapper {
    int batchInsertForwardIndex(@Param("list") List<Document> list);

    int batchInsertInvertedIndex(@Param("list") List<InvertedRecord> list);
}
