package com.peixinchen.searcher.indexer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.File;

@SpringBootTest
class IndexerApplicationTests {
    @Autowired
    private DocumentBuilder documentBuilder;

    @MockBean
    private Main main;

    @Test
    void documentBuilder() {
        File rootFile = new File("D:\\搜索引擎\\docs\\api");
        File file = new File("D:\\搜索引擎\\docs\\api\\index.html");

        Document document = documentBuilder.build(rootFile, file);

        String content = document.getContent();
        String[] words = content.split(" ");
        for (String word : words) {
            System.out.println(word);
        }
    }
}
