CREATE DATABASE `searcher` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

CREATE TABLE `searcher`.`forward_indexes` (
    `docid` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(100) NOT NULL,
    `url` varchar(200) NOT NULL,
    `content` longtext NOT NULL,
    PRIMARY KEY (`docid`)
) COMMENT='正排索引';

CREATE TABLE `searcher`.`inverted_indexes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `word` varchar(100) NOT NULL,
    `docid` int(11) NOT NULL,
    `weight` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) COMMENT='倒排索引';

-- 创建 MySQL 索引的步骤要等到我们把文档的倒排索引记录全部插入完成之后再添加好一些，否则会导致插入非常慢
ALTER TABLE `searcher`.`inverted_indexes` ADD INDEX `INDEX_word_weight` (`word` ASC, `weight` DESC);

