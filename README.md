# searcher

#### 介绍
比特项目 ：搜索引擎

使用 Spring 完成。

项目分成两部分，一部分是构建索引的操作，一部分是查找的操作。

使用数据库表保存构建好的正排、倒排索引

构建索引的模块是 indexer，原则上只需要运行一次，并且运行前确保表中一条记录都没有（可以通过 truncate 简单实现）。

查找的模块是 web，默认启动端口是 80

数据库相关的配置需要在运行前自行修改
文档所在目录需要在运行前自行修改

目前在 java 文档上做过简单的测试了。文档下载地址：https://www.oracle.com/java/technologies/javase-jdk8-doc-downloads.html
